package operations;

public class Add implements Operation {

    @Override
    public int calculate(int a, int b) {
        return a + b;
    }

    @Override
    public boolean canCalculate(String operation) {
        return operation.equals("add");
    }
}
