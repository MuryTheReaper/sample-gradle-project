package benchmark;

import data.Item;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import repo.InMemoryRepository;
import repoFactory.RepoItemProvider;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 2, time = 1, timeUnit = TimeUnit.SECONDS)
public class RepositoryBenchmark {

    @Benchmark
    public void addBenchmark(MyState state, Blackhole blackhole) {
        for (int i = 0; i < state.size; i++) {
            state.addRepository.add(new Item(i, i, i));
        }
        blackhole.consume(state.addRepository);
    }

    @Benchmark
    public void containsBenchmark(MyState state, Blackhole blackhole) {
        for (int i = 0; i < state.size * 2; i++) {
            state.containsRepository.contains(new Item(i, i, i));
        }
        blackhole.consume(state.containsRepository);
    }

    @Benchmark
    public void removeBenchmark(MyState state, Blackhole blackhole) {
        for (int i = 0; i < state.size; i++) {
            state.removeRepository.remove(new Item(i, i, i));
        }
        blackhole.consume(state.removeRepository);
    }

    @State(Scope.Thread)
    public static class MyState {
        @Param({"100000", "1024", "32", "5"})
        public int size;
        public InMemoryRepository<Item> containsRepository;
        public InMemoryRepository<Item> addRepository;
        public InMemoryRepository<Item> removeRepository;

        @Param({"ArrayList", "ConcurrentHashSet", "EclipseSet", "FastUtilHashSet", "HashSet", "TreeSet"})
        private String type;

        @Setup(Level.Iteration)
        public void setup() {
            RepoItemProvider repoItemProvider = new RepoItemProvider();
            Map<String, InMemoryRepository<Item>> repos = repoItemProvider.getRepos(type);
            addRepository = repos.get("add");
            containsRepository = repos.get("contain");
            removeRepository = repos.get("remove");

            for (int i = 0; i < size; i++) {
                containsRepository.add(new Item(i, i, i));
                removeRepository.add(new Item(i, i, i));
            }
        }

        @TearDown(Level.Iteration)
        public void tearDown() {
            addRepository = null;
            containsRepository = null;
            removeRepository = null;
        }
    }
}

