package benchmark;

import operations.StreamOperations;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@State(Scope.Benchmark)
public class StreamOperationsBenchmark {
    private final StreamOperations streamOperations = new StreamOperations();

    @State(Scope.Benchmark)
    public static class StateObject {
        @Param({"100", "1000000"})
        public int MAX_COUNT;
        List<Double> doubleList = new ArrayList<>();

        @Setup(Level.Iteration)
        public void doSetup() {
            for (int i = 0; i < MAX_COUNT; i++) {
                doubleList.add(Math.random() * MAX_COUNT);
            }
        }
    }

    @State(Scope.Benchmark)
    public static class StatePrimitive {
        @Param({"100", "1000000"})
        public int MAX_COUNT;
        DoubleArrayList doubleList = new DoubleArrayList();

        @Setup(Level.Iteration)
        public void doSetup() {
            for (int i = 0; i < MAX_COUNT; i++) {
                doubleList.add(Math.random() * MAX_COUNT);
            }
        }
    }

    @Benchmark
    public void sumDoubleObject(Blackhole consumer, StateObject object) {
        consumer.consume(streamOperations.sumDoubleObject(object.doubleList));
    }

    @Benchmark
    public void averageDoubleObject(Blackhole consumer, StateObject object) {
        consumer.consume(streamOperations.averageDoubleObject(object.doubleList));
    }

    @Benchmark
    public void topTenDoubleObject(Blackhole consumer, StateObject object) {
        consumer.consume(streamOperations.topTenDoubleObject(object.doubleList));
    }

    @Benchmark
    public void sumDoublePrimitive(Blackhole consumer, StatePrimitive primitive) {
        consumer.consume(streamOperations.sumDoublePrimitive(primitive.doubleList));
    }

    @Benchmark
    public void averageDoublePrimitive(Blackhole consumer, StatePrimitive primitive) {
        consumer.consume(streamOperations.averageDoublePrimitive(primitive.doubleList));
    }

    @Benchmark
    public void topTenDoublePrimitive(Blackhole consumer, StatePrimitive primitive) {
        consumer.consume(streamOperations.topTenDoublePrimitive(primitive.doubleList));
    }
}

