package repoFactory;

import repo.FastUtilBasedRepository;
import repo.InMemoryRepository;

import java.util.HashMap;
import java.util.Map;

public class FastUtilRepoFactory<T> implements RepoFactory<T> {
    @Override
    public Map<String, InMemoryRepository<T>> getReposMap() {
        Map<String, InMemoryRepository<T>> reposMap = new HashMap<>();
        reposMap.put("add", new FastUtilBasedRepository<>());
        reposMap.put("contain", new FastUtilBasedRepository<>());
        reposMap.put("remove", new FastUtilBasedRepository<>());
        return reposMap;
    }
}
