package repoFactory;

import repo.EclipseCollectionBasedRepository;
import repo.InMemoryRepository;

import java.util.HashMap;
import java.util.Map;

public class EclipseSetRepoFactory<T> implements RepoFactory<T> {

    @Override
    public Map<String, InMemoryRepository<T>> getReposMap() {
        Map<String, InMemoryRepository<T>> reposMap = new HashMap<>();
        reposMap.put("add", new EclipseCollectionBasedRepository<>());
        reposMap.put("contain", new EclipseCollectionBasedRepository<>());
        reposMap.put("remove", new EclipseCollectionBasedRepository<>());
        return reposMap;
    }
}
