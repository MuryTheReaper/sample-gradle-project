package operations;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;

public class StreamOperations {
    public BigDecimal sum(List<BigDecimal> bigDecimals){
        return bigDecimals.stream().
                reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().
                reduce(BigDecimal.ZERO, BigDecimal::add).
                divide(BigDecimal.valueOf(bigDecimals.size()), RoundingMode.HALF_UP);
    }

    public List<BigDecimal> topTen(List<BigDecimal> bigDecimals){
       return bigDecimals.stream().
                sorted(Comparator.reverseOrder()).
                limit(10).
                collect(Collectors.toList());
    }

    public Double sumDoubleObject(List<Double> doubleList) {
        return doubleList.stream().
                reduce(0.0, Double::sum);
    }

    public Double averageDoubleObject(List<Double> doubleList) {
        return doubleList.stream().
                reduce(0.0, Double::sum) / doubleList.size();
    }

    public List<Double> topTenDoubleObject(List<Double> doubleList) {
        return doubleList.stream().
                sorted(Comparator.reverseOrder()).
                limit(10).
                collect(Collectors.toList());
    }

    public double sumDoublePrimitive(DoubleArrayList doubleList) {
        return doubleList.stream().
                reduce(0.0, Double::sum);
    }

    public double averageDoublePrimitive(DoubleArrayList doubleList) {
        return doubleList.stream().
                reduce(0.0, Double::sum) / doubleList.size();
    }

    public DoubleArrayList topTenDoublePrimitive(DoubleArrayList doubleList) {
        return doubleList.stream().
                sorted(Comparator.reverseOrder()).
                limit(10).
                collect(Collectors.toCollection(DoubleArrayList::new));
    }

}

