package operations;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class StreamOperationsTest {

    StreamOperations streamOperations = new StreamOperations();

    @Test
    void sum() {
        List<BigDecimal> bigDecimals = List.of(BigDecimal.valueOf(1), BigDecimal.valueOf(2), BigDecimal.valueOf(3));

        BigDecimal sum = streamOperations.sum(bigDecimals);

        assertThat(sum).isEqualTo(BigDecimal.valueOf(6));
    }

    @Test
    void average() {
        List<BigDecimal> bigDecimals = List.of(BigDecimal.valueOf(1), BigDecimal.valueOf(3), BigDecimal.valueOf(4));

        BigDecimal average = streamOperations.average(bigDecimals);

        assertThat(average).isEqualTo(BigDecimal.valueOf(3));
    }

    @Test
    void topTen() {
        List<BigDecimal> bigDecimals = List.of(
                BigDecimal.valueOf(1), BigDecimal.valueOf(2), BigDecimal.valueOf(3),
                BigDecimal.valueOf(4), BigDecimal.valueOf(5), BigDecimal.valueOf(6),
                BigDecimal.valueOf(7), BigDecimal.valueOf(8), BigDecimal.valueOf(9),
                BigDecimal.valueOf(10), BigDecimal.valueOf(11), BigDecimal.valueOf(12)
        );

        List<BigDecimal> topTen = streamOperations.topTen(bigDecimals);

        assertThat(topTen).containsExactly(
                BigDecimal.valueOf(12), BigDecimal.valueOf(11), BigDecimal.valueOf(10),
                BigDecimal.valueOf(9), BigDecimal.valueOf(8), BigDecimal.valueOf(7),
                BigDecimal.valueOf(6), BigDecimal.valueOf(5), BigDecimal.valueOf(4),
                BigDecimal.valueOf(3)
        );
    }
}