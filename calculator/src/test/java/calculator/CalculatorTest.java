package calculator;

import operations.Add;
import operations.Divide;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class CalculatorTest {

    @Test
    @DisplayName("should calculate add")
    public void shouldCalculateAdd() {
        Calculator calculator = new Calculator(List.of(new Add()));
        assertThat(calculator.calculate(10, 5, "add"))
                .isEqualTo(15);
    }

    @Test
    @DisplayName("should calculate divide")
    public void shouldCalculateDivide() {
        Calculator calculator = new Calculator(List.of(new Divide()));
        assertThat(calculator.calculate(10, 5, "divide"))
                .isEqualTo(2);
    }

    @Test
    @DisplayName("should calculate all operations")
    public void shouldCalculateAllOperations() {
        Calculator calculator = new Calculator(List.of(new Add(), new Divide()));
        assertThat(calculator.calculate(10, 5, "add"))
                .isEqualTo(15);
        assertThat(calculator.calculate(10, 5, "divide"))
                .isEqualTo(2);
    }

    @Test
    public void shouldThrowExceptionWhenOperationNotFound() {
        Calculator calculator = new Calculator(List.of(new Add(), new Divide()));
        assertThatThrownBy(() -> calculator.calculate(10, 5, "subtract"))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Operation not found");
    }

}