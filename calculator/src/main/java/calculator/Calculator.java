package calculator;

import operations.Operation;

import java.util.List;

public class Calculator {

    List<Operation> operation;

    public Calculator(List<Operation> operation) {
        this.operation = operation;
    }

    public int calculate(int a, int b, String operation) {

        for (Operation op : this.operation) {
            if (op.canCalculate(operation)) {
                return op.calculate(a, b);
            }
        }
        throw new RuntimeException("Operation not found");
    }
}
