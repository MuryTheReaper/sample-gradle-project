package operations;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DivideTest {


    @Test
    public void shouldCalculateDivide() {
        Divide divide = new Divide();
        assertThat(divide.calculate(10, 5))
                .isEqualTo(2);
    }

    @Test
    public void shouldCalculateDivideNegative() {
        Divide divide = new Divide();
        assertThat(divide.calculate(-10, 5))
                .isEqualTo(-2);
    }

    @Test
    public void shouldBeAbleToCalculateDivide() {
        Divide divide = new Divide();
        assertThat(divide.canCalculate("divide"))
                .isTrue();
    }

    @Test
    public void shouldNotBeAbleToCalculateSubtract() {
        Divide divide = new Divide();
        assertThat(divide.canCalculate("subtract"))
                .isFalse();
    }

}