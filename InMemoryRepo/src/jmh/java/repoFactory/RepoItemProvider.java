package repoFactory;

import data.Item;
import repo.InMemoryRepository;

import java.util.HashMap;
import java.util.Map;

public class RepoItemProvider {

    private final Map<String, RepoFactory<Item>> repoFactoryMap;

    public RepoItemProvider() {
        this.repoFactoryMap = new HashMap<>();
        repoFactoryMap.put("ArrayList", new ArrayListRepoFactory<>());
        repoFactoryMap.put("EclipseSet", new EclipseSetRepoFactory<>());
        repoFactoryMap.put("ConcurrentHashSet", new ConcurrentHashRepoFactory<>());
        repoFactoryMap.put("FastUtilHashSet", new FastUtilRepoFactory<>());
        repoFactoryMap.put("HashSet", new HashSetRepoFactory<>());
        repoFactoryMap.put("TreeSet", new TreeSetRepoFactory<>());
    }

    public Map<String, InMemoryRepository<Item>> getRepos(String type) {
        RepoFactory<Item> repoFactory = repoFactoryMap.get(type);
        return repoFactory.getReposMap();
    }
}
