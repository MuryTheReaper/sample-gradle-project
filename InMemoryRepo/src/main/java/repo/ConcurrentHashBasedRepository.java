package repo;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashBasedRepository<T> implements InMemoryRepository<T> {

    private final ConcurrentHashMap<T, Object> map = new ConcurrentHashMap<>();

    @Override
    public void add(T t) {
        map.put(t, new Object());
    }

    @Override
    public boolean contains(T t) {
        return map.containsKey(t);
    }

    @Override
    public void remove(T t) {
        map.remove(t);
    }
}
