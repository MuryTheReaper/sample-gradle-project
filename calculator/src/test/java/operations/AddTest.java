package operations;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AddTest {

    @Test
    public void shouldCalculateAdd() {
        Add add = new Add();
        assertThat(add.calculate(10, 5))
                .isEqualTo(15);
    }

    @Test
    public void shouldCalculateAddNegative() {
        Add add = new Add();
        assertThat(add.calculate(-10, 5))
                .isEqualTo(-5);
    }

    @Test
    public void shouldBeAbleToCalculateAdd() {
        Add add = new Add();
        assertThat(add.canCalculate("add"))
                .isTrue();
    }

    @Test
    public void shouldNotBeAbleToCalculateSubtract() {
        Add add = new Add();
        assertThat(add.canCalculate("subtract"))
                .isFalse();
    }


}