package main;

import calculator.Calculator;
import operations.Add;
import operations.Divide;

import java.util.List;

public class AppStart {
    public static void main(String[] args) {

        Calculator fullCalculator = new Calculator(List.of(new Add(), new Divide()));

        System.out.println(fullCalculator.calculate(10, 5, "add"));
        System.out.println(fullCalculator.calculate(10, 5, "divide"));

    }
}
