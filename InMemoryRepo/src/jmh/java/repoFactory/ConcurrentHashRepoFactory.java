package repoFactory;

import repo.ConcurrentHashBasedRepository;
import repo.InMemoryRepository;

import java.util.HashMap;
import java.util.Map;

public class ConcurrentHashRepoFactory<T> implements RepoFactory<T> {
    @Override
    public Map<String, InMemoryRepository<T>> getReposMap() {
        Map<String, InMemoryRepository<T>> reposMap = new HashMap<>();
        reposMap.put("add", new ConcurrentHashBasedRepository<>());
        reposMap.put("contain", new ConcurrentHashBasedRepository<>());
        reposMap.put("remove", new ConcurrentHashBasedRepository<>());
        return reposMap;
    }
}
