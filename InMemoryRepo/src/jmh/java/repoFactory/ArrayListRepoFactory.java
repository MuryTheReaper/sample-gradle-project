package repoFactory;

import repo.ArrayListBasedRepository;
import repo.InMemoryRepository;

import java.util.HashMap;
import java.util.Map;

public class ArrayListRepoFactory<T> implements RepoFactory<T> {

    @Override
    public Map<String, InMemoryRepository<T>> getReposMap() {
        Map<String, InMemoryRepository<T>> reposMap = new HashMap<>();
        reposMap.put("add", new ArrayListBasedRepository<>());
        reposMap.put("contain", new ArrayListBasedRepository<>());
        reposMap.put("remove", new ArrayListBasedRepository<>());
        return reposMap;
    }
}
