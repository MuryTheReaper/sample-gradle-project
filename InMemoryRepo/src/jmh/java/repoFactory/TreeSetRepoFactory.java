package repoFactory;

import repo.InMemoryRepository;
import repo.TreeSetBasedRepository;

import java.util.HashMap;
import java.util.Map;

public class TreeSetRepoFactory<T> implements RepoFactory<T> {

    @Override
    public Map<String, InMemoryRepository<T>> getReposMap() {
        Map<String, InMemoryRepository<T>> reposMap = new HashMap<>();
        reposMap.put("add", new TreeSetBasedRepository<>());
        reposMap.put("contain", new TreeSetBasedRepository<>());
        reposMap.put("remove", new TreeSetBasedRepository<>());
        return reposMap;
    }
}
