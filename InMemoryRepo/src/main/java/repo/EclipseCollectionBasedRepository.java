package repo;

import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;

public class EclipseCollectionBasedRepository<T> implements InMemoryRepository<T> {

    private final MutableSet<T> list = UnifiedSet.newSet();

    @Override
    public void add(T t) {
        list.add(t);
    }

    @Override
    public boolean contains(T t) {
        return list.contains(t);
    }

    @Override
    public void remove(T t) {
        list.remove(t);
    }
}
