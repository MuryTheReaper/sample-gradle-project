package operations;

public interface Operation {

     int calculate(int a, int b);

     boolean canCalculate(String operation);


}
