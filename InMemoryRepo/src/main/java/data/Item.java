package data;

import java.util.Objects;

public class Item implements Comparable<Item> {

    int id;

    int price;

    int quantity;

    public Item(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return price == item.price && quantity == item.quantity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, quantity);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public int compareTo(Item item) {
        return Integer.compare(this.price * this.quantity, item.price * item.quantity);
    }
}
