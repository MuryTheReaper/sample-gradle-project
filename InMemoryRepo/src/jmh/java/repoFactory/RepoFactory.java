package repoFactory;

import repo.InMemoryRepository;

import java.util.Map;

public interface RepoFactory<T> {
    Map<String, InMemoryRepository<T>> getReposMap();
}
