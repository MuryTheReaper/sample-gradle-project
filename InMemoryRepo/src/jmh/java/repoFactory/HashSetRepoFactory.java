package repoFactory;

import repo.HashSetBasedRepository;
import repo.InMemoryRepository;

import java.util.HashMap;
import java.util.Map;

public class HashSetRepoFactory<T> implements RepoFactory<T> {

    @Override
    public Map<String, InMemoryRepository<T>> getReposMap() {
        Map<String, InMemoryRepository<T>> reposMap = new HashMap<>();
        reposMap.put("add", new HashSetBasedRepository<>());
        reposMap.put("contain", new HashSetBasedRepository<>());
        reposMap.put("remove", new HashSetBasedRepository<>());
        return reposMap;
    }
}
